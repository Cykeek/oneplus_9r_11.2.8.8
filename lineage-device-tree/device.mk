#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

# Enable updating of APEXes
$(call inherit-product, $(SRC_TARGET_DIR)/product/updatable_apex.mk)

# Include GSI keys
$(call inherit-product, $(SRC_TARGET_DIR)/product/gsi_keys.mk)

# A/B
$(call inherit-product, $(SRC_TARGET_DIR)/product/virtual_ab_ota.mk)

PRODUCT_PACKAGES += \
    android.hardware.boot@1.2-impl \
    android.hardware.boot@1.2-impl.recovery \
    android.hardware.boot@1.2-service

PRODUCT_PACKAGES += \
    update_engine \
    update_engine_sideload \
    update_verifier

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_system=true \
    POSTINSTALL_PATH_system=system/bin/otapreopt_script \
    FILESYSTEM_TYPE_system=ext4 \
    POSTINSTALL_OPTIONAL_system=true

AB_OTA_POSTINSTALL_CONFIG += \
    RUN_POSTINSTALL_vendor=true \
    POSTINSTALL_PATH_vendor=bin/checkpoint_gc \
    FILESYSTEM_TYPE_vendor=ext4 \
    POSTINSTALL_OPTIONAL_vendor=true

PRODUCT_PACKAGES += \
    checkpoint_gc \
    otapreopt_script

# fastbootd
PRODUCT_PACKAGES += \
    android.hardware.fastboot@1.1-impl-mock \
    fastbootd

# Health
PRODUCT_PACKAGES += \
    android.hardware.health@2.1-impl \
    android.hardware.health@2.1-service

# Overlays
PRODUCT_ENFORCE_RRO_TARGETS := *

# Partitions
PRODUCT_BUILD_SUPER_PARTITION := false
PRODUCT_USE_DYNAMIC_PARTITIONS := true

# Product characteristics
PRODUCT_CHARACTERISTICS := nosdcard

# Rootdir
PRODUCT_PACKAGES += \
    terminate_sniffer.sh \
    init.qcom.sh \
    init.qcom.efs.sync.sh \
    nfc_trasmit_stop.sh \
    init.qti.dcvs.sh \
    init.qcom.early_boot.sh \
    init.mdm.sh \
    init.qti.chg_policy.sh \
    init.qcom.sdio.sh \
    init.qti.qcv.sh \
    qca6234-service.sh \
    dump_sniffer.sh \
    init.qcom.usb.sh \
    init.class_main.sh \
    init.qcom.sensors.sh \
    init.crda.sh \
    init.qcom.coex.sh \
    init.qcom.post_boot.sh \
    launch_sniffer.sh \
    nfc_trasmit_start.sh \
    init.qcom.class_core.sh \
    startcontention.sh \
    init.qti.media.sh \

PRODUCT_PACKAGES += \
    fstab.qcom \
    init.qti.ufs.rc \
    init.target.rc \
    init.qcom.usb.rc \
    init.oem.rc \
    init.oem.minidump.sdx55m.rc \
    init.oem_ftm.rc \
    init.oem_rf.rc \
    init.oem.sec.rc \
    vendor.oem_ftm.rc \
    vendor.oem_ftm_svc_disable.rc \
    init.qcom.rc \
    init.oem.debug.rc \
    init.cust.rc \
    init.qcom.factory.rc \
    ftm_power_config.sh \
    ueventd.qcom.rc \
    init.recovery.qcom.rc \

PRODUCT_COPY_FILES += \
    $(LOCAL_PATH)/rootdir/etc/fstab.qcom:$(TARGET_VENDOR_RAMDISK_OUT)/first_stage_ramdisk/fstab.qcom

# Shipping API level
PRODUCT_SHIPPING_API_LEVEL := 30

# Soong namespaces
PRODUCT_SOONG_NAMESPACES += \
    $(LOCAL_PATH)

# Inherit the proprietary files
$(call inherit-product, vendor/oneplus/OnePlus9R/OnePlus9R-vendor.mk)
