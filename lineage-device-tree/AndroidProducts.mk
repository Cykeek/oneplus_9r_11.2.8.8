#
# Copyright (C) 2023 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_OnePlus9R.mk

COMMON_LUNCH_CHOICES := \
    lineage_OnePlus9R-user \
    lineage_OnePlus9R-userdebug \
    lineage_OnePlus9R-eng
