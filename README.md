## qssi-user 11 RKQ1.201112.002 2202232010 release-keys
- Manufacturer: oneplus
- Platform: kona
- Codename: OnePlus9R
- Brand: OnePlus
- Flavor: qssi-user
- Release Version: 11
- Kernel Version: 4.19.125
- Id: RKQ1.201112.002
- Incremental: 2202232010
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Treble Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: OnePlus/OnePlus9R_IND/OnePlus9R:11/RKQ1.201112.002/2202232010:user/release-keys
- OTA version: 
- Branch: qssi-user-11-RKQ1.201112.002-2202232010-release-keys
- Repo: oneplus_oneplus9r_dump
